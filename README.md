# PENCIL DURABILITY
___

Pencil Durability is a text editing program which allows you to write, delete, and edit text.

In addition, the program can keep track of both the lead and eraser material used when writing and erasing text. A user may sharpen the pencil when the point goes dull, and when the pencil runs out of lead or eraser it will no longer function as a pencil. 

Lastly, as in real life, when erasing a word from the middle of a sentence and replacing it with another word, the text will not magically shift and thus when writing words longer than those erased the new text will overlap and result in an "@" sign.

# REQUIREMENTS
___

`Python3.7`

There are no external dependencies outside of the Python standard library

# TO RUN THE PROJECT

___

1. GitLab Repository | [Link](https://gitlab.com/nrivlin/pencil-durability) 
2. In command line:

`$ git clone https://gitlab.com/nrivlin/pencil-durability.git`

`$ python3 pencil-durability/run.py`


## CODE FUNCTIONALITY:

**Create a new paper:**
 
`p1= paper.Paper()`

**Create a new pencil:**

`pen1= pencil.Pencil(model_durability=100, length=10, point_durability=30, eraser_durability = 50)`

**Write:**

`p1.write("Whatever text you like!", pen1)`

**Erase:**

`p1.erase_and_edit("text", pen1)`

**Edit:**

Optional third parameter for replacing the erased word.

Editing is dependant on having enough eraser to fully erase the replaced word.

`p1.erase_and_edit("you", pen1, insert="we")`

**Sharpen:**

Will set pencil point back to model_durability, reducing the pencil length by 1 with each use. 


`pen1.sharpen()`

**Reflect:**

Will show all text that has been written on a sheet of paper.

Optional parameter p: If p = 'Y', the method will print the text in addition to returning the paper text.

`p1.reflect()`  or  `p1.reflect("Y")`


# TO RUN THE TESTS

___

## PENCIL TEST:

`$ python3 pencil-durability/test_pencil.py`

## PAPER TEST:

`$ python3 pencil-durability/test_paper.py`



