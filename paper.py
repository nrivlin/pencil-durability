class Paper:

    def __init__(self):
        self.text = ""

    def reflect(self, p='N'):
        '''Reflects the text written on a given sheet of paper'''
        if p.upper() == 'Y':
            print(self.text)
        return self.text

    def write(self, string, pencil):
        for letter in string:
            letter_points = self.check_points(letter)
            if letter_points <= pencil.point_durability:
                self.text += letter
                pencil.point_durability -= letter_points
            else:
                self.text += " "

    def check_points(self, string):
        '''Calculates the points to be degraded from the pencil's point durability'''
        if string.isupper():
            return 2
        elif string.islower():
            return 1
        elif string.isspace() or string == "\n":
            return 0
        else:
            return 1

    def erase_and_edit(self, string, pencil, insert=None):
        '''Erases last occurrence of text word provided,
        optional insert of new string to take place or deleted word'''
        word_list = self.text.split(" ")[::-1]
        for index, word in enumerate(word_list):
            if word == string and insert is not None:
                text_to_replace = list(" ".join(word_list[index::-1])) # Create a list of signs for replacement
                if pencil.eraser_durability >= len(word):
                    pencil.eraser_durability -= len(word)
                    for i in range(0,len(insert)):
                        insert_letter_points = self.check_points(str(insert[i]))
                        if i < len(word):
                            if insert_letter_points <= pencil.point_durability:
                                text_to_replace[i] = insert[i]
                                pencil.point_durability -= insert_letter_points
                            else:
                                text_to_replace[i] = " "
                        else:
                            if insert_letter_points <= pencil.point_durability and text_to_replace[i] == " ":
                                text_to_replace[i] = insert[i]
                                pencil.point_durability -= insert_letter_points
                            elif insert_letter_points <= pencil.point_durability:
                                text_to_replace[i] = "@"
                                pencil.point_durability -= insert_letter_points
                            else:
                                text_to_replace[i] = " "
                else:
                    return "Cannot perform edit!"
                self.text = f"{' '.join(word_list[:index:-1])} {''.join(text_to_replace)}"  # Undo reverse to original string after deletion

            elif word == string:
                word_list[index] = "" # Reset the word up for deletion
                for letter in word[::-1]: # Erase by appending spaces from the end (reversed)
                    if pencil.eraser_durability > 0:
                        word_list[index] += " "
                        pencil.eraser_durability -= 1
                    else:
                        word_list[index] += letter
                word_list[index] = word_list[index][::-1] # Undo reversed append to print correctly
                self.text = " ".join(word_list[::-1])  # Undo reverse to original string after deletion
                break