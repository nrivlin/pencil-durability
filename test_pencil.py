import pencil
import paper
import unittest

class TestPencil(unittest.TestCase):

    def test_pencil_attributes(self):
        '''Assert instantiation'''
        pen1 = pencil.Pencil(model_durability=100, length=10, point_durability=10, eraser_durability=50)
        self.assertEqual(100, pen1.model_durability)
        self.assertEqual(10, pen1.length)
        self.assertEqual(10, pen1.point_durability)
        self.assertEqual(50, pen1.eraser_durability)

    def test_pencil_durability(self):
        '''Assert point degradation when writing text'''
        p1 = paper.Paper()
        pen1 = pencil.Pencil(model_durability=100, length=10, point_durability=10, eraser_durability=50)
        p1.write("abc", pen1)
        self.assertEqual("abc", p1.reflect())
        self.assertEqual(7, pen1.point_durability)
        p1.write("ABC", pen1)
        self.assertEqual("abcABC", p1.reflect())
        self.assertEqual(1, pen1.point_durability)
        p1.write("def", pen1)
        self.assertEqual("abcABCd  ", p1.reflect())
        self.assertEqual(0, pen1.point_durability)

    def test_sharpen(self):
        '''Assert point reset when sharpening pencil, and reduces length of pencil'''
        pen1 = pencil.Pencil(model_durability=100, length=10, point_durability=10, eraser_durability=50)
        pen1.sharpen()
        self.assertEqual(100, pen1.point_durability)
        self.assertEqual(9, pen1.length)

if __name__ == '__main__':
    unittest.main()