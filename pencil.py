class Pencil:

    def __init__(self,model_durability=100, length=10, point_durability=50, eraser_durability=50):
        self.model_durability = model_durability
        self.length = length
        self.point_durability = point_durability
        self.eraser_durability = eraser_durability

    def sharpen(self):
        '''Resets pencil's point durability to the model's initial value'''
        if self.length >= 1:
            self.point_durability = self.model_durability
            self.length -= 1

