import paper
import pencil
import unittest

class TestPaper(unittest.TestCase):

    def test_paper_attributes(self):
        '''Assert instantiation'''
        p1 = paper.Paper()
        self.assertEqual("", p1.text)

    def test_reflect(self):
        '''Assert the reflect method'''
        p1 = paper.Paper()
        self.assertEqual("", p1.reflect())

    def test_write(self):
        '''Assert write functionality'''
        p1 = paper.Paper()
        pen1 = pencil.Pencil(model_durability=100, length=10, point_durability=30, eraser_durability=50)
        p1.write("Hello ", pen1)
        self.assertEqual("Hello ", p1.reflect())
        p1.write("World", pen1)
        self.assertEqual("Hello World", p1.reflect())
        p1.write("!!!", pen1)
        self.assertEqual("Hello World!!!", p1.reflect())

    def test_check_points(self):
        '''Check for the value of each character'''
        p1 = paper.Paper()
        self.assertEqual(1, p1.check_points("h"))
        self.assertEqual(2, p1.check_points("H"))
        self.assertEqual(0, p1.check_points(" "))
        self.assertEqual(0, p1.check_points("\n"))
        self.assertEqual(1, p1.check_points("!"))
        self.assertEqual(1, p1.check_points("?"))

    def test_erase_and_edit(self):
        '''Assert text deletion and insertion'''
        p1 = paper.Paper()
        pen1 = pencil.Pencil(model_durability=100, length=10, point_durability=30, eraser_durability=50)
        p1.write("abc def ghi jkl mno", pen1)
        p1.erase_and_edit("jkl", pen1)
        self.assertEqual("abc def ghi     mno", p1.reflect())
        p1.write(" abc def", pen1)
        self.assertEqual("abc def ghi     mno abc def", p1.reflect())
        p1.erase_and_edit("abc", pen1)
        self.assertEqual("abc def ghi     mno     def", p1.reflect())
        self.assertEqual(44, pen1.eraser_durability)
        p1.write(" hello", pen1)
        pen1.eraser_durability = 4
        self.assertEqual("abc def ghi     mno     def hello", p1.reflect())
        p1.erase_and_edit("hello", pen1)
        self.assertEqual("abc def ghi     mno     def h    ", p1.reflect())
        pen1.eraser_durability = 10
        p1.text = "abcdefghijk abcdefghijk"
        p1.erase_and_edit("abcdefghijk", pen1)
        self.assertEqual("abcdefghijk a          ", p1.reflect())
        pen1.eraser_durability = 3
        p1.text = "Buffalo Bill"
        p1.erase_and_edit("Bill", pen1)
        self.assertEqual("Buffalo B   ", p1.reflect())
        p2 = paper.Paper()
        pen2 = pencil.Pencil(model_durability=100, length=10, point_durability=100, eraser_durability=50)
        p2.write("Have a nice day mate!", pen2)
        p2.erase_and_edit("nice", pen2, insert="good")
        self.assertEqual("Have a good day mate!", p2.reflect())
        p2.erase_and_edit("good", pen2, insert="wonderful")
        self.assertEqual("Have a wonde@@@lmate!", p2.reflect())
        self.assertEqual(42, pen2.eraser_durability)
        self.assertEqual(69, pen2.point_durability)

if __name__ == '__main__':
    unittest.main()