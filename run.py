import paper
import pencil

def run():

    p1 = paper.Paper()
    pen1 = pencil.Pencil(model_durability=100, length=10, point_durability=30, eraser_durability=50)
    p1.write("This is my kata!", pen1)
    p1.reflect('Y')
    p1.erase_and_edit("my", pen1, insert="your")
    p1.reflect('Y')
    pen1.sharpen()
    p1.write(" Now it reads funny..", pen1)
    p1.reflect('Y')
    p1.erase_and_edit("reads", pen1)
    p1.reflect('Y')


if __name__ == '__main__':
    run()